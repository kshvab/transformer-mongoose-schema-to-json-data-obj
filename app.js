const mongoose = require("mongoose");
require("mongoose-schema-jsonschema")(mongoose);

const Schema = mongoose.Schema;

const BookSchema = new Schema({
    locations_id: { type: String, required: true },
    measured_from: { type: Number, required: true },
    measured_to: { type: Number, required: true },
    value: { type: Number, required: true },
});

const jsonSchema = BookSchema.jsonSchema();

console.dir(jsonSchema, { depth: null });
